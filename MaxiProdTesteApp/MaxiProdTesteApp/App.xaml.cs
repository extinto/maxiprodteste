using MaxiProdTesteApp.Models;
using MaxiProdTesteApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace MaxiProdTesteApp
{
	public partial class App : Application
	{
        private HomeMasterDetailPage masterDetail;

        public App ()
		{
			InitializeComponent();
            masterDetail = new HomeMasterDetailPage();
            AppNavigation = new NavigationPage(new HomePage());
            masterDetail.Detail = AppNavigation;
			MainPage = masterDetail;
            if(CartProduct == null || !CartProduct.Equals(""))
            {
                CartProduct = new ObservableCollection<Product>();
            }

            if (CartShopping == null || !CartShopping.Equals(""))
            {
                CartShopping = new ObservableCollection<Shoppings>();
            }
        }

        public static NavigationPage AppNavigation {get; set;}

        public static ObservableCollection<Product> CartProduct { get; set; }

        public static ObservableCollection<Shoppings> CartShopping { get; set; }

        public void NavigateToPage(ContentPage page)
        {
            AppNavigation = new NavigationPage(page);
            masterDetail.Detail = AppNavigation;
            masterDetail.IsPresented = false;
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
