﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MaxiProdTesteApp.Models
{
    public class Product
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int UnitPrice { get; set; }

        public int Amount { get; set; }

        public int Total { get; set; }
    }
}
