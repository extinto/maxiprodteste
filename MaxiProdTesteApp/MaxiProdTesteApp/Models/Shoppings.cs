﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MaxiProdTesteApp.Models
{
    public class Shoppings
    {
        public int Id { get; set; }

        public int Total { get; set; }

        public ObservableCollection<Product> ProductList { get; set; }
    }
}
