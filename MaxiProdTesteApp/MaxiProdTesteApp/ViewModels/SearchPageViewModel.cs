﻿using MaxiProdTesteApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MaxiProdTesteApp.ViewModels
{

    public class SearchPageViewModel : BaseViewModel
    {
        private int _code;
        private int _amount;
        private int _somaTotal;
        private int _total;
        private Product _product;
        private int idSoma = 0;
        private Shoppings _shopping;

        public SearchPageViewModel()
        {
            Product = new Product();
            CalculateCommand = new Command(ExecuteCalculate);
            AddCommand = new Command(ExecuteAdd);
            ShoppingCommand = new Command(ExecuteShopping);
            ShoppingList = new ObservableCollection<Product>();
            //ShoppingList = App.CartProduct;
        }
        public ICommand CalculateCommand { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand ShoppingCommand { get; set; }
        public ObservableCollection<Product> ShoppingList { get; set; }

        public Shoppings Shopping
        {
            get
            {
                return _shopping;
            }
            set
            {
                setProperty(ref _shopping, value);

            }
        }

        private void ExecuteAdd()
        {
            Product = new Product();
            Product.Total = SomaTotal;
            ShoppingList.Add(Product);
        }

        private void ExecuteShopping()
        {
            Shopping = new Shoppings();
            foreach (var e in ShoppingList)
            {
                Total += e.Total;
            }
            Shopping.ProductList = ShoppingList;
            Shopping.Total = Total;
            App.CartShopping.Add(Shopping);
            foreach (var item in App.CartShopping)
            {
                idSoma++;
                item.Id = idSoma;
            }
            idSoma = 0;
        }

        private void ExecuteCalculate()
        {
            SomaTotal = Product.UnitPrice * Amount;
            Product.Amount = Amount;
        }

        private void SearchExecute()
        {
            foreach(var item in App.CartProduct)
            {
                if(item.Id == Code)
                {
                    Product = item;
                }
            }
            
        }

        public int Code
        {
            get
            {
                return _code;
            }
            set
            {
                setProperty(ref _code, value);
                SearchExecute();
            }
        }

        public int Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                setProperty(ref _amount, value);

            }
        }

        public int Total
        {
            get
            {
                return _total;
            }
            set
            {
                setProperty(ref _total, value);

            }
        }

        public int SomaTotal
        {
            get
            {
                return _somaTotal;
            }
            set
            {
                setProperty(ref _somaTotal, value);
            }
        }

        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                setProperty(ref _product, value);
            }
        }
    }
}
