﻿using MaxiProdTesteApp.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MaxiProdTesteApp.ViewModels
{
    public class MasterMenuViewModel
    {
        public MasterMenuViewModel()
        {
            NavigateCommand = new Command(ExecuteNavigate);
            NavigateHomeCommand = new Command(ExecuteNavigateHome);
            NavigateSearchCommand = new Command(ExecuteNavigateSearch);
            NavigateShoppingCommand = new Command(ExecuteNavigateShopping);
        }
        public ICommand NavigateCommand { get; set; }
        public ICommand NavigateSearchCommand { get; set; }
        public ICommand NavigateHomeCommand { get; set; }
        public ICommand NavigateShoppingCommand { get; set; }

        private void ExecuteNavigateShopping()
        {
            ((App)Application.Current).NavigateToPage(new ShoppingDetail());
        }

        private void ExecuteNavigate()
        {
            ((App)Application.Current).NavigateToPage(new ShoppingListPage());
        }

        private void ExecuteNavigateSearch()
        {
            ((App)Application.Current).NavigateToPage(new SearchPage());
        }

        private void ExecuteNavigateHome()
        {
            ((App)Application.Current).NavigateToPage(new HomePage());
        }


    }
    
}
