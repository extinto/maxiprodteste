﻿using MaxiProdTesteApp.Models;
using MaxiProdTesteApp.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MaxiProdTesteApp.ViewModels
{
    public class MainVIewModel : BaseViewModel
    {
        private int _productCode;
        private string _descriptionProduct;
        private int _unitPriceProduct;
        private Product _product;
        private ObservableCollection<Product> _productList;

        public MainVIewModel()
        {
            ShoppingList = new ObservableCollection<Product>();
            SaveCommand = new Command(ExecuteCommand);
            ShoppingCommand = new Command(ExecuteShoppingCommand);
            Product = new Product();
        }

        public ICommand SaveCommand { get; set; }
        public ICommand ShoppingCommand { get; set; }


        public ObservableCollection<Product> ShoppingList
        {
            get
            {
                return _productList;
            }
            set
            {
                setProperty(ref _productList, value);
                
            }
        }

        private void ExecuteShoppingCommand()
        {
            App.AppNavigation.PushAsync(new SearchPage());
        }
        private void ExecuteCommand()
        {
            Product = new Product();
            Product.Id = ProductCode;
            Product.Description = DescriptionProduct;
            Product.UnitPrice = UnitPriceProduct;
            ShoppingList.Add(Product);
            App.CartProduct = ShoppingList;
            RefreshView();
            ProductCode = 0;
            DescriptionProduct = null;
            UnitPriceProduct = 0;
        }

        public void RefreshView()
        {
            OnProperty("ShoppingList");
            OnProperty("Product");
            OnProperty("ProductCode");
            OnProperty("DescriptionProduct");
            OnProperty("UnitPriceProduct");
            OnProperty("ProductList");
        }

        public ObservableCollection<Product> ProductList
        {
            get
            {
                return _productList;
            }

            set
            {
                setProperty(ref _productList, value);
            }
        }

        public Product Product
        {
            get
            {
                return _product;
            }
            set
            {
                setProperty(ref _product, value);
            }
        }

        public int ProductCode
        {
            get
            {
                return _productCode;
            }
            set
            {
                setProperty(ref _productCode, value);
            }
        }

        public string DescriptionProduct
        {
            get
            {
                return _descriptionProduct;
            }
            set
            {
                setProperty(ref _descriptionProduct, value);
            }
        }

        public int UnitPriceProduct
        {
            get
            {
                return _unitPriceProduct;
            }
            set
            {
                setProperty(ref _unitPriceProduct, value);
            }
        }
    }
}
