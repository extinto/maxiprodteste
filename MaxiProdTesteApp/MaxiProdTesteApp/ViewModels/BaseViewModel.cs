﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace MaxiProdTesteApp.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperty([CallerMemberName]string propertyname = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        public bool setProperty<T>(ref T s, T valor, [CallerMemberName]string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(s, valor))
            {
                return false;
            }

            s = valor;
            OnProperty(propertyName);
            return true;
        }
    }
}
