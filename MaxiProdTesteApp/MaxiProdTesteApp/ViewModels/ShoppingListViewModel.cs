﻿using MaxiProdTesteApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace MaxiProdTesteApp.ViewModels
{
    public class ShoppingListViewModel : BaseViewModel
    {
        private int _total;
        private int _amount;
        private int soma = 0;
        private int somVar = 0;
        private int idSoma = 0;
        private Shoppings _shopping;

        public ShoppingListViewModel()
        {
            ShoppingList = new ObservableCollection<Product>();
            ShoppingList = App.CartProduct;
            CalculateTotal();
            SaveShoppingCommand = new Command(ExecuteShoppingCommand);
        }
        public ICommand SaveShoppingCommand { get; set; }

        public ObservableCollection<Product> ShoppingList { get; set; }

        private void ExecuteShoppingCommand()
        {
            Shopping = new Shoppings();
            Shopping.ProductList = ShoppingList;
            Shopping.Total = Total;
            App.CartShopping.Add(Shopping);
            foreach(var item in App.CartShopping)
            {
                idSoma++;
                item.Id = idSoma;
            }
            idSoma = 0;
        }

        private void CalculateTotal()
        {
            foreach(var item in ShoppingList)
            {
                soma += item.UnitPrice;
            }
            Total = soma;
        }

        public int Amount
        {
            get
            {
                return _amount;
            }
            set
            {
                setProperty(ref _amount, value);
                if(value == 0)
                {
                    value = 1;
                }
            }
        }

        public int Total
        {
            get
            {
                return _total;
            }
            set
            {
                setProperty(ref _total,  value);

            }
        }

        public Shoppings Shopping
        {
            get
            {
                return _shopping;
            }
            set
            {
                setProperty(ref _shopping, value);

            }
        }

    }
}
