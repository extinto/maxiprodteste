﻿using MaxiProdTesteApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace MaxiProdTesteApp.ViewModels
{
    public class ShoppingDetailViewModel : BaseViewModel
    {
        private bool _isTrue;
        private Shoppings _selectedItem;

        public ShoppingDetailViewModel()
        {
            ShoppingList = new ObservableCollection<Shoppings>();
            ShoppingList = App.CartShopping;
        }
        public ObservableCollection<Shoppings> ShoppingList { get; set; }

        public bool IsTrue
        {
            get
            {
                return _isTrue;
            }
            set
            {
                setProperty(ref _isTrue, value);
            }
        }

        public Shoppings SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                setProperty(ref _selectedItem, value);
            }
        }
    }
}
