﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace MaxiProdTesteApp.Views
{
    public class HomeMasterDetailPage : MasterDetailPage
    {
        public HomeMasterDetailPage()
        {
            Master = new MasterMenu();
        } 
    }
}
