﻿using MaxiProdTesteApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MaxiProdTesteApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
            BindingContext = new MainVIewModel();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((MainVIewModel)BindingContext).RefreshView();
        }
    }
}